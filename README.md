# ged

A UI for Gross Error Detection Service

## Запуск сборки проекта
/// Запустить сборку проекта единоразово
flutter packages pub run build_runner build --delete-conflicting-outputs

/// Запустить сборку проекта и следить за изменениями
flutter packages pub run build_runner watch --delete-conflicting-outputs
