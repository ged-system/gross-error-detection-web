import 'package:flutter/material.dart';
import 'package:ged/core/utilities/device_info.dart';

/// Адаптирует виджет под различные размеры экрана.
class AdaptiveContainer extends StatelessWidget {
  /// Контекст.
  final BuildContext context;

  /// Виджет, отображаемый по умолчанию для всех размеров окна.
  final Widget? defaultContent;

  /// Виджет, отображаемый для маленьких экранов (телефон).
  final Widget? mobile;

  /// Виджет, отображаемый для средних экранов (планшет).
  final Widget? tablet;

  /// Виджет, отображаемый для больших экранов (монитор).
  final Widget? monitor;

  const AdaptiveContainer({
    Key? key,
    required this.context,
    this.defaultContent,
    this.mobile,
    this.tablet,
    this.monitor,
  }) : assert(defaultContent != null || (monitor != null && mobile != null));

  @override
  Widget build(BuildContext context) {
    final _formFactor = Device.get(this.context);

    final defaultContent = this.defaultContent;
    final monitor = this.monitor;
    final tablet = this.tablet;
    final mobile = this.mobile;

    switch (_formFactor) {
      case FormFactorType.Monitor:
        {
          return Center(
            child: Container(
              constraints: BoxConstraints(
                minWidth: Device.minMonitorBreakpoint.toDouble(),
                maxWidth: Device.maxMonitorBreakpoint.toDouble(),
              ),
              child: monitor ?? defaultContent,
            ),
          );
        }
      case FormFactorType.Tablet:
        {
          return Center(
            child: Container(
              constraints: BoxConstraints(
                minWidth: Device.minTabletBreakpoint.toDouble(),
                maxWidth: Device.maxTabletBreakpoint.toDouble(),
              ),
              child: tablet ?? defaultContent,
            ),
          );
        }
      case FormFactorType.Phone:
      default:
        {
          return Center(
            child: mobile ?? defaultContent,
          );
        }
    }
  }
}
