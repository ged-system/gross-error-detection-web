class DataStack<T> {
  final List<T> _stack = [];

  void add(T value) {
    _stack.add(value);
  }

  T? receive() {
    if (_stack.isEmpty) {
      return null;
    }

    final element = _stack.last;
    _stack.removeLast();
    return element;
  }

  void clear() {
    _stack.clear();
  }

  int length() {
    return _stack.length;
  }
}
