import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:universal_platform/universal_platform.dart';

enum FormFactorType { Monitor, Phone, Tablet }

class DeviceOS {
  ///Приложение запущено на IOS
  static bool isIOS = UniversalPlatform.isIOS;

  ///Приложение запущено на Android
  static bool isAndroid = UniversalPlatform.isAndroid;

  ///Приложение запущено на MacOS
  static bool isMacOS = UniversalPlatform.isMacOS;

  ///Приложение запущено на Linux
  static bool isLinux = UniversalPlatform.isLinux;

  ///Приложение запущено на Windows
  static bool isWindows = UniversalPlatform.isWindows;

  ///Приложение запущено в браузере
  static bool isWeb = kIsWeb;

  ///Приложение запущено на рабочем столе
  static bool get isDesktop => isWindows || isMacOS || isLinux;

  ///Приложение запущено на мобильном теле
  static bool get isMobile => isAndroid || isIOS;

  static bool get isDesktopOrWeb => isDesktop || isWeb;

  static bool get isMobileOrWeb => isMobile || isWeb;
}

class Device {
  // Get the device form factor as best we can.
  // Otherwise we will use the screen size to determine which class we fall into.
  // Откуда брал пограничные значения https://docs.microsoft.com/en-us/windows/apps/design/layout/screen-sizes-and-breakpoints-for-responsive-design
  static FormFactorType get(BuildContext context) {
    double shortestSide = MediaQuery.of(context).size.width;
    if (shortestSide <= maxPhoneBreakpoint) return FormFactorType.Phone;
    if (shortestSide >= minTabletBreakpoint && shortestSide <= maxTabletBreakpoint) return FormFactorType.Tablet;
    if (shortestSide >= minMonitorBreakpoint) return FormFactorType.Monitor;
    return FormFactorType.Monitor;
  }

  static int minPhoneBreakpoint = 0;
  static int maxPhoneBreakpoint = 640;
  static int minTabletBreakpoint = 641;
  static int maxTabletBreakpoint = 1007;
  static int minMonitorBreakpoint = 1008;
  static int maxMonitorBreakpoint = double.maxFinite.toInt();

  // Shortcuts for various mobile device types

  static bool isPhone(BuildContext context) => get(context) == FormFactorType.Phone;

  static bool isTablet(BuildContext context) => get(context) == FormFactorType.Tablet;

  static bool isMonitor(BuildContext context) => get(context) == FormFactorType.Monitor;
}

class Screen {
  static double get _ppi => (UniversalPlatform.isAndroid || UniversalPlatform.isIOS) ? 150 : 96;

  static bool isLandscape(BuildContext c) => MediaQuery.of(c).orientation == Orientation.landscape;

  //PIXELS
  static Size size(BuildContext c) => MediaQuery.of(c).size;

  static double width(BuildContext c) => size(c).width;

  static double height(BuildContext c) => size(c).height;

  static double diagonal(BuildContext c) {
    Size s = size(c);
    return sqrt((s.width * s.width) + (s.height * s.height));
  }

  //INCHES
  static Size inches(BuildContext c) {
    Size pxSize = size(c);
    return Size(pxSize.width / _ppi, pxSize.height / _ppi);
  }

  static double widthInches(BuildContext c) => inches(c).width;

  static double heightInches(BuildContext c) => inches(c).height;

  static double diagonalInches(BuildContext c) => diagonal(c) / _ppi;
}
