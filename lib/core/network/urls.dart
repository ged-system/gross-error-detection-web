class Urls {
  static const String _port = '5036';
  static const String _url = 'localhost';

  static const String serverUrl = 'http://$_url:$_port';

  static const String grossErrorDetectionController = 'GrossErrorDetection';
  static const String grossErrorDetectionInteractive = 'api/$grossErrorDetectionController/GrossErrorDetectionInteractive';
  static const String grossErrorReductionInteractive = 'api/$grossErrorDetectionController/GrossErrorReductionInteractive';
}
