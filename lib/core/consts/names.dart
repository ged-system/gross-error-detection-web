class Names {
  static const String uploadButton = 'Загрузить';
  static const String runButton = 'Поиск';
  static const String rerunButton = 'Перезапуск';
  static const String reduceButton = 'Устранить ошибки';
  static const String backButton = 'Назад';

  static const String noData = 'Нет данных';
  static const String selectFlow = 'Выберите поток';
  static const String selectedFlows = 'Выбранные потоки';
  static const String detectedFlows = 'Обнаруженные потоки с ошибками';
  static const String solvedFlows = 'Балансовая схема с устраненными ошибками';

  static const String globalTestName = 'Global Test';
  static const String glrName = 'GLR-тест';

}
