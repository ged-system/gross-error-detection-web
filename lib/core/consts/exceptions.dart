class Exceptions {
  static const fileDataException = 'Не удалось данные файла';
  static const jsonException = 'Не удалось десериализировать файл';
}