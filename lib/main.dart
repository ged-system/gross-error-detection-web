import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/presentation/presentation.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gross Error Detection',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      scrollBehavior: _MyCustomScrollBehavior(),
      home: const ErrorReduction(),
    );
  }
}

class _MyCustomScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}
