import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'request_status_enum.g.dart';

class RequestStatusEnum extends EnumClass {
  static const RequestStatusEnum work = _$work;
  static const RequestStatusEnum load = _$load;
  static const RequestStatusEnum error = _$error;

  const RequestStatusEnum._(String name) : super(name);

  static BuiltSet<RequestStatusEnum> get values => _$requestStatusEnumValues;
  static RequestStatusEnum valueOf(String name) => _$requestStatusEnumValueOf(name);
}