// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flow_constraints.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FlowConstraints extends FlowConstraints {
  @override
  final double? upperBound;
  @override
  final double? lowerBound;

  factory _$FlowConstraints([void Function(FlowConstraintsBuilder)? updates]) =>
      (new FlowConstraintsBuilder()..update(updates)).build();

  _$FlowConstraints._({this.upperBound, this.lowerBound}) : super._();

  @override
  FlowConstraints rebuild(void Function(FlowConstraintsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FlowConstraintsBuilder toBuilder() =>
      new FlowConstraintsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FlowConstraints &&
        upperBound == other.upperBound &&
        lowerBound == other.lowerBound;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, upperBound.hashCode), lowerBound.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('FlowConstraints')
          ..add('upperBound', upperBound)
          ..add('lowerBound', lowerBound))
        .toString();
  }
}

class FlowConstraintsBuilder
    implements Builder<FlowConstraints, FlowConstraintsBuilder> {
  _$FlowConstraints? _$v;

  double? _upperBound;
  double? get upperBound => _$this._upperBound;
  set upperBound(double? upperBound) => _$this._upperBound = upperBound;

  double? _lowerBound;
  double? get lowerBound => _$this._lowerBound;
  set lowerBound(double? lowerBound) => _$this._lowerBound = lowerBound;

  FlowConstraintsBuilder();

  FlowConstraintsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _upperBound = $v.upperBound;
      _lowerBound = $v.lowerBound;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FlowConstraints other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FlowConstraints;
  }

  @override
  void update(void Function(FlowConstraintsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$FlowConstraints build() {
    final _$result = _$v ??
        new _$FlowConstraints._(upperBound: upperBound, lowerBound: lowerBound);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
