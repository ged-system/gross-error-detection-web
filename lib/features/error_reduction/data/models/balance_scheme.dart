import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:ged/features/error_reduction/data/models/flow.dart';

part 'balance_scheme.g.dart';

/// Объект балансовой схемы
abstract class BalanceScheme implements Built<BalanceScheme, BalanceSchemeBuilder> {
  /// Имя схемы
  String? get name;

  /// Коллекция потоков
  BuiltList<Flow> get flows;

  BalanceScheme._();
  factory BalanceScheme([void Function(BalanceSchemeBuilder) updates]) = _$BalanceScheme;
}