// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'balance_scheme.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BalanceScheme extends BalanceScheme {
  @override
  final String? name;
  @override
  final BuiltList<Flow> flows;

  factory _$BalanceScheme([void Function(BalanceSchemeBuilder)? updates]) =>
      (new BalanceSchemeBuilder()..update(updates)).build();

  _$BalanceScheme._({this.name, required this.flows}) : super._() {
    BuiltValueNullFieldError.checkNotNull(flows, 'BalanceScheme', 'flows');
  }

  @override
  BalanceScheme rebuild(void Function(BalanceSchemeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BalanceSchemeBuilder toBuilder() => new BalanceSchemeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BalanceScheme && name == other.name && flows == other.flows;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, name.hashCode), flows.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BalanceScheme')
          ..add('name', name)
          ..add('flows', flows))
        .toString();
  }
}

class BalanceSchemeBuilder
    implements Builder<BalanceScheme, BalanceSchemeBuilder> {
  _$BalanceScheme? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  ListBuilder<Flow>? _flows;
  ListBuilder<Flow> get flows => _$this._flows ??= new ListBuilder<Flow>();
  set flows(ListBuilder<Flow>? flows) => _$this._flows = flows;

  BalanceSchemeBuilder();

  BalanceSchemeBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _flows = $v.flows.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BalanceScheme other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BalanceScheme;
  }

  @override
  void update(void Function(BalanceSchemeBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BalanceScheme build() {
    _$BalanceScheme _$result;
    try {
      _$result = _$v ?? new _$BalanceScheme._(name: name, flows: flows.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'flows';
        flows.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'BalanceScheme', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
