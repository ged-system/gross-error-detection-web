import 'package:built_value/built_value.dart';
import 'package:ged/features/error_reduction/data/models/flow_constraints.dart';

part 'flow.g.dart';

/// Объект потока
abstract class Flow implements Built<Flow, FlowBuilder> {
  /// Идентификатор потока
  String? get id;

  /// Наименование потока
  String? get name;

  /// Узел источник
  String? get sourceId;

  /// Узел назначения
  String? get destinationId;

  /// Измеренное значение
  double? get measured;

  /// Погрешность измерения
  double? get tolerance;

  /// Показатель измеряется ли поток
  bool? get isMeasured;

  /// Показатель включен ли поток в схему
  bool? get isExcluded;

  /// Метрологические ограничения
  FlowConstraints? get metrologicRange;

  /// Технологические ограничения
  FlowConstraints? get technologicRange;

  /// Показатель, что поток искусственный
  bool? get isArtificial;

  Flow._();
  factory Flow([void Function(FlowBuilder) updates]) = _$Flow;
}