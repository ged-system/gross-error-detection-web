import 'package:built_value/built_value.dart';

part 'flow_constraints.g.dart';

/// Объект ограничений
abstract class FlowConstraints implements Built<FlowConstraints, FlowConstraintsBuilder> {
  /// Верхние ограничения
  double? get upperBound;

  /// Нижние ограничения
  double? get lowerBound;

  FlowConstraints._();
  factory FlowConstraints([void Function(FlowConstraintsBuilder) updates]) = _$FlowConstraints;
}