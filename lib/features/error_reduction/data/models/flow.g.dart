// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flow.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Flow extends Flow {
  @override
  final String? id;
  @override
  final String? name;
  @override
  final String? sourceId;
  @override
  final String? destinationId;
  @override
  final double? measured;
  @override
  final double? tolerance;
  @override
  final bool? isMeasured;
  @override
  final bool? isExcluded;
  @override
  final FlowConstraints? metrologicRange;
  @override
  final FlowConstraints? technologicRange;
  @override
  final bool? isArtificial;

  factory _$Flow([void Function(FlowBuilder)? updates]) =>
      (new FlowBuilder()..update(updates)).build();

  _$Flow._(
      {this.id,
      this.name,
      this.sourceId,
      this.destinationId,
      this.measured,
      this.tolerance,
      this.isMeasured,
      this.isExcluded,
      this.metrologicRange,
      this.technologicRange,
      this.isArtificial})
      : super._();

  @override
  Flow rebuild(void Function(FlowBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FlowBuilder toBuilder() => new FlowBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Flow &&
        id == other.id &&
        name == other.name &&
        sourceId == other.sourceId &&
        destinationId == other.destinationId &&
        measured == other.measured &&
        tolerance == other.tolerance &&
        isMeasured == other.isMeasured &&
        isExcluded == other.isExcluded &&
        metrologicRange == other.metrologicRange &&
        technologicRange == other.technologicRange &&
        isArtificial == other.isArtificial;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc($jc(0, id.hashCode), name.hashCode),
                                        sourceId.hashCode),
                                    destinationId.hashCode),
                                measured.hashCode),
                            tolerance.hashCode),
                        isMeasured.hashCode),
                    isExcluded.hashCode),
                metrologicRange.hashCode),
            technologicRange.hashCode),
        isArtificial.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Flow')
          ..add('id', id)
          ..add('name', name)
          ..add('sourceId', sourceId)
          ..add('destinationId', destinationId)
          ..add('measured', measured)
          ..add('tolerance', tolerance)
          ..add('isMeasured', isMeasured)
          ..add('isExcluded', isExcluded)
          ..add('metrologicRange', metrologicRange)
          ..add('technologicRange', technologicRange)
          ..add('isArtificial', isArtificial))
        .toString();
  }
}

class FlowBuilder implements Builder<Flow, FlowBuilder> {
  _$Flow? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _sourceId;
  String? get sourceId => _$this._sourceId;
  set sourceId(String? sourceId) => _$this._sourceId = sourceId;

  String? _destinationId;
  String? get destinationId => _$this._destinationId;
  set destinationId(String? destinationId) =>
      _$this._destinationId = destinationId;

  double? _measured;
  double? get measured => _$this._measured;
  set measured(double? measured) => _$this._measured = measured;

  double? _tolerance;
  double? get tolerance => _$this._tolerance;
  set tolerance(double? tolerance) => _$this._tolerance = tolerance;

  bool? _isMeasured;
  bool? get isMeasured => _$this._isMeasured;
  set isMeasured(bool? isMeasured) => _$this._isMeasured = isMeasured;

  bool? _isExcluded;
  bool? get isExcluded => _$this._isExcluded;
  set isExcluded(bool? isExcluded) => _$this._isExcluded = isExcluded;

  FlowConstraintsBuilder? _metrologicRange;
  FlowConstraintsBuilder get metrologicRange =>
      _$this._metrologicRange ??= new FlowConstraintsBuilder();
  set metrologicRange(FlowConstraintsBuilder? metrologicRange) =>
      _$this._metrologicRange = metrologicRange;

  FlowConstraintsBuilder? _technologicRange;
  FlowConstraintsBuilder get technologicRange =>
      _$this._technologicRange ??= new FlowConstraintsBuilder();
  set technologicRange(FlowConstraintsBuilder? technologicRange) =>
      _$this._technologicRange = technologicRange;

  bool? _isArtificial;
  bool? get isArtificial => _$this._isArtificial;
  set isArtificial(bool? isArtificial) => _$this._isArtificial = isArtificial;

  FlowBuilder();

  FlowBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _sourceId = $v.sourceId;
      _destinationId = $v.destinationId;
      _measured = $v.measured;
      _tolerance = $v.tolerance;
      _isMeasured = $v.isMeasured;
      _isExcluded = $v.isExcluded;
      _metrologicRange = $v.metrologicRange?.toBuilder();
      _technologicRange = $v.technologicRange?.toBuilder();
      _isArtificial = $v.isArtificial;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Flow other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Flow;
  }

  @override
  void update(void Function(FlowBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Flow build() {
    _$Flow _$result;
    try {
      _$result = _$v ??
          new _$Flow._(
              id: id,
              name: name,
              sourceId: sourceId,
              destinationId: destinationId,
              measured: measured,
              tolerance: tolerance,
              isMeasured: isMeasured,
              isExcluded: isExcluded,
              metrologicRange: _metrologicRange?.build(),
              technologicRange: _technologicRange?.build(),
              isArtificial: isArtificial);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metrologicRange';
        _metrologicRange?.build();
        _$failedField = 'technologicRange';
        _technologicRange?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Flow', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
