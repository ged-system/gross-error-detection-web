import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';

part 'ged_reduction_response_dto.g.dart';

/// Объект переноса данных результата устранения грубых ошибок
abstract class GedReductionResponseDto implements Built<GedReductionResponseDto, GedReductionResponseDtoBuilder> {

  BuiltList<ReducedFlowDto>? get reducedFlows;

  double? get imbalanceBefore;

  double? get imbalanceAfter;

  double? get errorsCount;

  GedReductionResponseDto._();
  factory GedReductionResponseDto([void Function(GedReductionResponseDtoBuilder) updates]) = _$GedReductionResponseDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(GedReductionResponseDto.serializer, this) as Map<String, dynamic>;
  }

  static GedReductionResponseDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(GedReductionResponseDto.serializer, json);
  }

  static Serializer<GedReductionResponseDto> get serializer => _$gedReductionResponseDtoSerializer;
}