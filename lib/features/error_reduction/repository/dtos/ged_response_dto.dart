import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'package:ged/features/error_reduction/repository/dtos/dtos.dart';

part 'ged_response_dto.g.dart';

/// Объект переноса данных запроса поиска грубых ошибок
abstract class GedResponseDto implements Built<GedResponseDto, GedResponseDtoBuilder> {
  /// Коллекция потоков балансовой схемы
  BuiltList<FlowDto>? get flows;

  /// Коллекция раннее добавленных потоков с ошибками
  BuiltList<FlowDto>? get errorFlows;

  /// Потоки с найденными ошибками
  BuiltList<GlrSampleFlowDto>? get sampleFlows;

  /// Показатель, определяющий, что решение найдено
  bool? get isSolved;

  GedResponseDto._();

  factory GedResponseDto([void Function(GedResponseDtoBuilder) updates]) = _$GedResponseDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(GedResponseDto.serializer, this) as Map<String, dynamic>;
  }

  static GedResponseDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(GedResponseDto.serializer, json);
  }

  static Serializer<GedResponseDto> get serializer => _$gedResponseDtoSerializer;
}
