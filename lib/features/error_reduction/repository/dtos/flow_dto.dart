import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ged/features/error_reduction/repository/dtos/constraints_dto.dart';
import 'package:ged/features/error_reduction/repository/dtos/dtos.dart';

part 'flow_dto.g.dart';

abstract class FlowDto implements Built<FlowDto, FlowDtoBuilder> {
  /// Идентификатор потока
  String? get id;

  /// Наименование потока
  String? get name;

  /// Узел источник
  String? get sourceId;

  /// Узел назначения
  String? get destinationId;

  /// Измеренное значение
  double? get measured;

  /// Погрешность измерения
  double? get tolerance;

  /// Показатель измеряется ли поток
  bool? get isMeasured;

  /// Показатель включен ли поток в схему
  bool? get isExcluded;

  /// Метрологические ограничения
  ConstraintsDto? get metrologicRange;

  /// Технологические ограничения
  ConstraintsDto? get technologicRange;

  /// Показатель, что поток искусственный
  bool? get isArtificial;

  FlowDto._();
  factory FlowDto([void Function(FlowDtoBuilder) updates]) = _$FlowDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(FlowDto.serializer, this) as Map<String, dynamic>;
  }

  static FlowDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(FlowDto.serializer, json);
  }

  static Serializer<FlowDto> get serializer => _$flowDtoSerializer;
}