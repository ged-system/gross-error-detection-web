import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ged/features/error_reduction/repository/dtos/dtos.dart';

part 'balance_scheme_dto.g.dart';

abstract class BalanceSchemeDto implements Built<BalanceSchemeDto, BalanceSchemeDtoBuilder> {
  /// Балансовая схема
  BuiltList<FlowDto>? get flows;

  /// Настройки
  SettingsDto? get settings;

  BalanceSchemeDto._();

  factory BalanceSchemeDto([void Function(BalanceSchemeDtoBuilder) updates]) = _$BalanceSchemeDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(BalanceSchemeDto.serializer, this) as Map<String, dynamic>;
  }

  static BalanceSchemeDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(BalanceSchemeDto.serializer, json);
  }

  static Serializer<BalanceSchemeDto> get serializer => _$balanceSchemeDtoSerializer;
}
