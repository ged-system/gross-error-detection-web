import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'dtos.dart';

part 'constraints_dto.g.dart';

abstract class ConstraintsDto implements Built<ConstraintsDto, ConstraintsDtoBuilder> {
  /// Верхние ограничения
  double? get upperBound;

  /// Нижние ограничения
  double? get lowerBound;

  ConstraintsDto._();
  factory ConstraintsDto([void Function(ConstraintsDtoBuilder) updates]) = _$ConstraintsDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(ConstraintsDto.serializer, this) as Map<String, dynamic>;
  }

  static ConstraintsDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(ConstraintsDto.serializer, json);
  }

  static Serializer<ConstraintsDto> get serializer => _$constraintsDtoSerializer;
}