import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'dtos.dart';

part 'settings_dto.g.dart';

abstract class SettingsDto implements Built<SettingsDto, SettingsDtoBuilder> {
  /// Выбранные ограчичения
  String? get constraints;

  /// Солвер глобального теста
  String? get globalTestSolver;

  /// Солвер баланса
  String? get balanceSolver;

  /// Тип ошибок
  BuiltList<String>? get errors;

  SettingsDto._();

  factory SettingsDto([void Function(SettingsDtoBuilder) updates]) = _$SettingsDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(SettingsDto.serializer, this) as Map<String, dynamic>;
  }

  static SettingsDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(SettingsDto.serializer, json);
  }

  static Serializer<SettingsDto> get serializer => _$settingsDtoSerializer;
}
