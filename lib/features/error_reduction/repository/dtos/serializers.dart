import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:ged/features/error_reduction/repository/dtos/constraints_dto.dart';
import 'package:ged/features/error_reduction/repository/dtos/settings_dto.dart';

import 'dtos.dart';

part 'serializers.g.dart';

@SerializersFor([
  BalanceSchemeDto,
  FlowDto,
  ConstraintsDto,
  SettingsDto,
  GedRequestDto,
  GedResponseDto,
  GlrSampleFlowDto,
  GedReductionResponseDto,
  ReducedFlowDto,
  GrossErrorType,
])
final Serializers serializers = (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
