import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'package:ged/features/error_reduction/repository/repository.dart';

part 'gross_error_type.g.dart';

class GrossErrorType extends EnumClass {
  static const GrossErrorType measure = _$measure;
  static const GrossErrorType leak = _$leak;
  static const GrossErrorType unaccounted = _$unaccounted;

  String get translates => _translates[this] ?? (throw StateError('No translate for GrossErrorType.$name'));

  static const _translates = {
    measure: 'Ошибка измерения',
    leak: 'Утечка',
    unaccounted: 'Неучтенный поток',
  };

  const GrossErrorType._(String name) : super(name);

  static BuiltSet<GrossErrorType> get values => _$grossErrorTypeValues;

  static GrossErrorType valueOf(String name) => _$grossErrorTypeValueOf(name);

  String? serialize() {
    return serializers.serializeWith(GrossErrorType.serializer, this) as String;
  }

  static GrossErrorType? deserialize(String string) {
    return serializers.deserializeWith(GrossErrorType.serializer, string);
  }

  static Serializer<GrossErrorType> get serializer => _$grossErrorTypeSerializer;
}
