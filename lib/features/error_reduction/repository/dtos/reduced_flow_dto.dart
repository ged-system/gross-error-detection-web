import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';

part 'reduced_flow_dto.g.dart';

/// Объект переноса данных потока после устранения грубой ошибки
abstract class ReducedFlowDto implements Built<ReducedFlowDto, ReducedFlowDtoBuilder> {
  String? get id;

  String? get name;

  String? get sourceId;

  String? get destinationId;

  double? get measured;

  double? get reconciledMeasured;

  double? get differenceValue;

  bool? get isArtificial;

  String? get errorType;

  ReducedFlowDto._();
  factory ReducedFlowDto([void Function(ReducedFlowDtoBuilder) updates]) = _$ReducedFlowDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(ReducedFlowDto.serializer, this) as Map<String, dynamic>;
  }

  static ReducedFlowDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(ReducedFlowDto.serializer, json);
  }

  static Serializer<ReducedFlowDto> get serializer => _$reducedFlowDtoSerializer;
}