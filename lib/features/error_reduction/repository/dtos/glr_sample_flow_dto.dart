import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'package:ged/features/error_reduction/repository/dtos/flow_dto.dart';

import 'package:ged/features/error_reduction/repository/dtos/serializers.dart';

part 'glr_sample_flow_dto.g.dart';

/// Объект переноса данных потока с результатом GLR-теста и глобального теста для балансовой схемы с ним
abstract class GlrSampleFlowDto implements Built<GlrSampleFlowDto, GlrSampleFlowDtoBuilder> {
  /// Значение глобального теста
  double? get globalTestValue;

  /// Значение GLR-теста
  double? get glrValue;

  /// Объект переноса данных потока
  FlowDto? get flow;

  GlrSampleFlowDto._();

  factory GlrSampleFlowDto([void Function(GlrSampleFlowDtoBuilder) updates]) = _$GlrSampleFlowDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(GlrSampleFlowDto.serializer, this) as Map<String, dynamic>;
  }

  static GlrSampleFlowDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(GlrSampleFlowDto.serializer, json);
  }

  static Serializer<GlrSampleFlowDto> get serializer => _$glrSampleFlowDtoSerializer;
}
