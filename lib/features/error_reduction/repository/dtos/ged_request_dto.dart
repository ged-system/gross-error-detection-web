import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'package:ged/features/error_reduction/repository/dtos/dtos.dart';

part 'ged_request_dto.g.dart';

/// Объект переноса данных запроса поиск грубых ошибок
abstract class GedRequestDto implements Built<GedRequestDto, GedRequestDtoBuilder> {
  /// Коллекция потоков балансовой схемы
  BuiltList<FlowDto>? get flows;

  /// Коллекция раннее добавленных потоков с ошибками
  BuiltList<FlowDto>? get errorFlows;

  /// Выбранный поток с ошибкой
  GlrSampleFlowDto? get sampleFlow;

  GedRequestDto._();

  factory GedRequestDto([void Function(GedRequestDtoBuilder) updates]) = _$GedRequestDto;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(GedRequestDto.serializer, this) as Map<String, dynamic>;
  }

  static GedRequestDto? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(GedRequestDto.serializer, json);
  }

  static Serializer<GedRequestDto> get serializer => _$gedRequestDtoSerializer;
}
