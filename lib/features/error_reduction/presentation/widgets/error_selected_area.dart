import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';

import 'package:ged/core/core.dart';

class ErrorSelectedArea extends StatelessWidget {
  final List<FlowDto> errorFlows;

  const ErrorSelectedArea({Key? key, required this.errorFlows}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final flows = errorFlows.map((flow) => Padding(
      padding: const EdgeInsets.all(4.0),
      child: ErrorSelectedFlow(flow: flow),
    )).toList();

    return Column(
      children: [
        const Text(
          Names.selectedFlows,
          style: TextStyle(fontWeight: FontWeight.w700),
        ),
        ConstrainedBox(
          constraints: const BoxConstraints(
            maxHeight: 240,
            minHeight: 150,
          ),
          child: ListView(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            children: flows,
          ),
        ),
      ],
    );
  }
}
