import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';

class ReducedErrorFlow extends StatelessWidget {
  final ReducedFlowDto flow;

  const ReducedErrorFlow({Key? key, required this.flow}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final decoration = BoxDecoration(
      borderRadius: BorderRadius.circular(8),
      border: Border.all(color: Theme.of(context).primaryColor),
      color: flow.errorType?.isNotEmpty == true ? Colors.redAccent : Colors.transparent,
    );

    return AnimatedContainer(
      duration: const Duration(milliseconds: 500),
      decoration: decoration,
      width: 150,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: IconButton(
                  icon: const Icon(Icons.description),
                  onPressed: () {
                    final info = Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SelectableText('Идентификатор: ${flow.id}'),
                        SelectableText('Узел источник: ${flow.sourceId}'),
                        SelectableText('Узел назначения: ${flow.destinationId}'),
                        SelectableText('Измереннное значение: ${flow.measured}'),
                        SelectableText('Сбалансированное значение: ${flow.reconciledMeasured}'),
                      ],
                    );
                    _showFlowDialog(context, info);
                  },
                  splashRadius: 15.0,
                  color: Theme.of(context).primaryColor.withOpacity(0.3),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SelectableText('Идентификатор: ${flow.id}'),
                    SelectableText('Измеренное значение: ${flow.measured}'),
                    SelectableText('Сбалансрованное значение: ${flow.reconciledMeasured}'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showFlowDialog(BuildContext context, Widget content) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Информация о потоке'),
          content: SizedBox(height: 100, child: content),
          actions: <Widget>[
            TextButton(
              child: const Text('Закрыть'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
