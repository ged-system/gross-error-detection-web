import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';
import 'package:provider/provider.dart';

class ResultedArea extends StatefulWidget {
  const ResultedArea({Key? key}) : super(key: key);

  @override
  State<ResultedArea> createState() => _ResultedAreaState();
}

class _ResultedAreaState extends State<ResultedArea> {
  ErrorReductionBloc get bloc => Provider.of<ErrorReductionBloc>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<GedReductionResponseDto>(
        stream: bloc.reduceErrorsStream,
        builder: (context, snapshot) {
          if (bloc.hideResultedArea) {
            return Container();
          }

          if (!snapshot.hasData) {
            return Container();
          }

          final data = snapshot.data;
          if (data == null) {
            return Container();
          }

          return ReducedErrorsArea(gedReductionResponse: data);
        });
  }
}
