import 'package:flutter/material.dart';

import 'package:ged/features/error_reduction/error_reduction.dart';

import 'package:ged/core/core.dart';

class ReducedErrorsArea extends StatelessWidget {
  final GedReductionResponseDto gedReductionResponse;

  const ReducedErrorsArea({Key? key, required this.gedReductionResponse}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final flows = gedReductionResponse.reducedFlows
            ?.map((flow) => Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: ReducedErrorFlow(flow: flow),
                ))
            .toList() ??
        <Widget>[];

    if (flows.isEmpty) {
      return Container();
    }

    return Column(
      children: [
        const Text(
          Names.solvedFlows,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [SelectableText(
              'Значение дисбаланса до поиска грубых ошибок: ${gedReductionResponse.imbalanceBefore}',
              style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            ),
              SelectableText(
                'Значение дисбаланса после поиска грубых ошибок: ${gedReductionResponse.imbalanceAfter}',
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                ),
              ),
              SelectableText(
                'Количество ошибок: ${gedReductionResponse.errorsCount}',
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                ),
              ),],
          ),
        ),
        ConstrainedBox(
          constraints: const BoxConstraints(
            maxHeight: 250,
            minHeight: 150,
          ),
          child: ListView(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            children: flows,
          ),
        ),
      ],
    );
  }
}
