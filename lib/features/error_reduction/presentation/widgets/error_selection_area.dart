import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';

import 'package:ged/core/core.dart';

class ErrorSelectionArea extends StatelessWidget {
  final List<GlrSampleFlowDto> flows;

  const ErrorSelectionArea({Key? key, required this.flows}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
          Names.selectFlow,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        ConstrainedBox(
          constraints: const BoxConstraints(
            maxHeight: 240,
            minHeight: 150,
          ),
          child: ListView.builder(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: flows.length,
            itemBuilder: (BuildContext context, int index) {
              final sampleFlow = flows.elementAt(index);
              return Padding(
                padding: const EdgeInsets.all(5.0),
                child: ErrorSelectionFlow(
                  number: index,
                  sampleFlow: sampleFlow,
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
