import 'package:flutter/material.dart';
import 'package:ged/core/consts/names.dart';

class BalanceSchemeUploadButton extends StatelessWidget {
  final VoidCallback? onPressed;

  const BalanceSchemeUploadButton({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: ElevatedButton(
          child: const Text(Names.uploadButton),
          onPressed: onPressed,
        ),
      ),
    );
  }
}
