import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/data/data.dart';

class BalanceSchemeInfo extends StatelessWidget {
  final BalanceScheme? balanceScheme;

  const BalanceSchemeInfo({Key? key, this.balanceScheme}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final name = balanceScheme?.name ?? '';

    final flows = balanceScheme?.flows;

    if (flows == null || flows.isEmpty) {
      return Container();
    }

    final widgets = flows
        .map(
          (entity) => ExpansionTile(
            title: Text('Поток: ${entity.name}'),
            expandedCrossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SelectableText('Идентификатор: ${entity.id}'),
              SelectableText('Узел источник: ${entity.sourceId}'),
              SelectableText('Узел назначения: ${entity.destinationId}'),
              SelectableText('Измереннное значение: ${entity.measured}'),
              SelectableText('Погрешность: ${entity.tolerance}'),
            ],
          ),
        )
        .toList();

    return Container(
      constraints: const BoxConstraints(
        minWidth: 300,
      ),
      child: InkWell(
        splashColor: Theme.of(context).primaryColor,
        onTap: () {
          _showBalanceSchemeDialog(context, widgets);
        },
        child: SizedBox(
          height: double.infinity,
          child: Align(
            alignment: Alignment.center,
            child: Text(
              'Балансовая схема: $name\nКоличество потоков: ${flows.length}',
            ),
          ),
        ),
      ),
      // child: ListView(
      //   children:
      // ),
    );
  }

  Future<void> _showBalanceSchemeDialog(BuildContext context, List<Widget> widgets) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Балансовая схема'),
          content: SizedBox(
            width: 300,
            child: SingleChildScrollView(
              child: ListBody(
                children: widgets,
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Закрыть'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
