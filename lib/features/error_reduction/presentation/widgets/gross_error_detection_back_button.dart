import 'package:flutter/material.dart';

class GrossErrorDetectionBackButton extends StatelessWidget {
  final String label;
  final VoidCallback? onPressed;

  const GrossErrorDetectionBackButton({Key? key, required this.label, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(primary: Colors.blueGrey);

    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: ElevatedButton(
          child: Text(label),
          style: style,
          onPressed: onPressed,
        ),
      ),
    );
  }
}
