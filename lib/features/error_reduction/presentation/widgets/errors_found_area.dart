import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/repository/dtos/flow_dto.dart';

import 'package:ged/core/core.dart';

import 'package:ged/features/error_reduction/presentation/presentation.dart';

class ErrorsFoundArea extends StatelessWidget {
  final List<FlowDto> errorFlows;

  const ErrorsFoundArea({Key? key, required this.errorFlows}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final flows = errorFlows.map((flow) => Padding(
      padding: const EdgeInsets.all(4.0),
      child: ErrorFoundFlow(flow: flow),
    )).toList();

    return Column(
      children: [
        const Text(
          Names.detectedFlows,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        ConstrainedBox(
          constraints: const BoxConstraints(
            maxHeight: 240,
            minHeight: 150,
          ),
          child: ListView(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            children: flows,
          ),
        ),
      ],
    );
  }
}
