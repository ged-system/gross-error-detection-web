import 'package:flutter/material.dart';

import 'package:ged/core/core.dart';

class GrossErrorDetectionButton extends StatelessWidget {
  final VoidCallback? onPressed;

  const GrossErrorDetectionButton({Key? key, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(primary: Colors.redAccent);

    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: ElevatedButton(
          child: const Text(Names.runButton),
          style: style,
          onPressed: onPressed,
        ),
      ),
    );
  }
}
