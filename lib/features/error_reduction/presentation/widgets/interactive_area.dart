import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';
import 'package:provider/provider.dart';

class InteractiveArea extends StatefulWidget {
  const InteractiveArea({Key? key}) : super(key: key);

  @override
  State<InteractiveArea> createState() => _InteractiveAreaState();
}

class _InteractiveAreaState extends State<InteractiveArea> {
  ErrorReductionBloc get bloc => Provider.of<ErrorReductionBloc>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<GedResponseDto>(
        stream: bloc.gedInteractiveStream,
        builder: (context, snapshot) {
          if (bloc.hideInteractiveArea) {
            return Container();
          }

          if (!snapshot.hasData) {
            return Container();
          }

          final gedResponse = snapshot.data;
          if (gedResponse == null) {
            return Container();
          }

          // Получаем выбранные потоки
          final errorFlows = gedResponse.errorFlows?.toList() ?? <FlowDto>[];

          if (gedResponse.isSolved == true) {
            return ErrorsFoundArea(errorFlows: errorFlows);
          }

          // Получаем коллекцию потоков для выбора
          final sampleFlows = gedResponse.sampleFlows?.toList() ?? <GlrSampleFlowDto>[];

          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (sampleFlows.isNotEmpty)
                ErrorSelectionArea(
                  flows: sampleFlows.toList(),
                ),
              if (errorFlows.isNotEmpty)
                ErrorSelectedArea(
                  errorFlows: errorFlows.toList(),
                ),
            ],
          );
        });
  }
}
