import 'package:flutter/material.dart';

import 'package:ged/core/core.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';

import 'package:provider/provider.dart';

class ErrorSelectionFlow extends StatefulWidget {
  final int number;
  final GlrSampleFlowDto sampleFlow;

  const ErrorSelectionFlow({Key? key, required this.number, required this.sampleFlow}) : super(key: key);

  @override
  State<ErrorSelectionFlow> createState() => _ErrorSelectionFlowState();
}

class _ErrorSelectionFlowState extends State<ErrorSelectionFlow> {
  ErrorReductionBloc get bloc => Provider.of<ErrorReductionBloc>(context, listen: false);

  var _selected = false;

  var _stressed = false;

  @override
  void dispose() {
    super.dispose();
    _selected = false;
    _stressed = false;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        var selectedFlow = GlrSampleFlowDto();
        if (!_selected) {
          selectedFlow = widget.sampleFlow;
        }
        _selected = !_selected;

        bloc.selectFlow(selectedFlow);
      },
      onHover: (value) {
        setState(() {
          _stressed = value;
        });
      },
      borderRadius: BorderRadius.circular(8),
      child: StreamBuilder<GlrSampleFlowDto>(
          stream: bloc.selectedFlowStream,
          builder: (context, snapshot) {
            final color = snapshot.data?.flow?.id == widget.sampleFlow.flow?.id
                ? Theme.of(context).primaryColor
                : _stressed
                    ? Theme.of(context).primaryColor.withOpacity(0.6)
                    : Colors.transparent;

            final decoration = BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Theme.of(context).primaryColor),
              color: color,
            );

            return AnimatedContainer(
              duration: const Duration(milliseconds: 500),
              decoration: decoration,
              width: 150,
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: IconButton(
                          icon: const Icon(Icons.description),
                          onPressed: () {
                            final info = Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SelectableText('Идентификатор: ${widget.sampleFlow.flow?.id}'),
                                SelectableText('Узел источник: ${widget.sampleFlow.flow?.sourceId}'),
                                SelectableText('Узел назначения: ${widget.sampleFlow.flow?.destinationId}'),
                                SelectableText('Измереннное значение: ${widget.sampleFlow.flow?.measured}'),
                                SelectableText('Погрешность: ${widget.sampleFlow.flow?.tolerance}'),
                              ],
                            );
                            _showFlowDialog(context, info);
                          },
                          splashRadius: 15.0,
                          color: Theme.of(context).primaryColor.withOpacity(0.3),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            DecoratedBox(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                    width: 4.0,
                                    color: widget.sampleFlow.globalTestValue! < 1.0
                                        ? Colors.greenAccent
                                        : Colors.transparent),
                              ),
                              child: Container(
                                padding: const EdgeInsets.all(8.0),
                                width: double.infinity,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    const Text('${Names.globalTestName}: '),
                                    Text(widget.sampleFlow.globalTestValue!.toString()),
                                  ],
                                ),
                              ),
                            ),
                            const Text('${Names.glrName}: '),
                            Text(widget.sampleFlow.glrValue.toString()),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }

  Future<void> _showFlowDialog(BuildContext context, Widget content) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Информация о потоке'),
          content: SizedBox(height: 100, child: content),
          actions: <Widget>[
            TextButton(
              child: const Text('Закрыть'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
