import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/error_reduction.dart';

import 'package:ged/core/core.dart';
import 'package:provider/provider.dart';

class BalanceSchemeArea extends StatefulWidget {
  const BalanceSchemeArea({Key? key}) : super(key: key);

  @override
  State<BalanceSchemeArea> createState() => _BalanceSchemeAreaState();
}

class _BalanceSchemeAreaState extends State<BalanceSchemeArea> {
  ErrorReductionBloc get bloc => Provider.of<ErrorReductionBloc>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    final decoration = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      border: Border.all(color: Theme.of(context).primaryColor),
    );

    return Container(
      constraints: const BoxConstraints(
        minWidth: 300,
        maxHeight: 220,
        minHeight: 200,
      ),
      decoration: decoration,
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          StreamBuilder<BalanceScheme>(
              stream: bloc.balanceSchemeStream,
              builder: (context, balanceSchemeSnapshot) {
                final balanceScheme = balanceSchemeSnapshot.data;

                return StreamBuilder<GedResponseDto>(
                    stream: bloc.gedInteractiveStream,
                    builder: (context, gedInteractiveSnapshot) {
                      final scheme = gedInteractiveSnapshot.data?.flows?.toList();

                      final errorFlows = gedInteractiveSnapshot.data?.errorFlows?.toList() ?? <FlowDto>[];

                      final isSolved = gedInteractiveSnapshot.data?.isSolved == true;

                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 120,
                            child: BalanceSchemeUploadButton(
                              onPressed: () {
                                bloc.pickFiles();
                              },
                            ),
                          ),
                          SizedBox(
                            width: 120,
                            child: StreamBuilder<GlrSampleFlowDto>(
                                stream: bloc.selectedFlowStream,
                                builder: (context, glrSampleFlowSnapshot) {
                                  final selectedFlow = glrSampleFlowSnapshot.data;

                                  final startGedButton = balanceScheme == null
                                      ? Container()
                                      : BalanceSchemeRunButton(
                                          label: errorFlows.isNotEmpty || selectedFlow != null
                                              ? Names.rerunButton
                                              : Names.runButton,
                                          onPressed: () {
                                            bloc.startGed(balanceScheme);
                                          },
                                        );

                                  return startGedButton;
                                }),
                          ),
                          SizedBox(
                            width: 120,
                            child: errorFlows.isEmpty
                                ? Container()
                                : GrossErrorDetectionBackButton(
                                    label: Names.backButton,
                                    onPressed: () {
                                      bloc.stepBack();
                                    },
                                  ),
                          ),
                          SizedBox(
                            width: 120,
                            child: StreamBuilder<GlrSampleFlowDto>(
                                stream: bloc.selectedFlowStream,
                                builder: (context, glrSampleFlowSnapshot) {
                                  final selectedFlow = glrSampleFlowSnapshot.data;
                                  if (selectedFlow == null) {
                                    return Container();
                                  }

                                  return GrossErrorDetectionButton(
                                    onPressed: selectedFlow.flow == null || scheme == null
                                        ? null
                                        : () {
                                            bloc.continueGed(scheme, errorFlows, selectedFlow);
                                          },
                                  );
                                }),
                          ),
                          SizedBox(
                            width: 120,
                            child: !isSolved || scheme == null
                                ? Container()
                                : GrossErrorReductionButton(
                                    onPressed: () {
                                      bloc.reduceErrors(scheme, errorFlows);
                                    },
                                  ),
                          ),
                        ],
                      );
                    });
              }),
          VerticalDivider(
            color: Theme.of(context).primaryColor,
            thickness: 2.0,
            indent: 10,
            endIndent: 10,
          ),
          StreamBuilder<BalanceScheme>(
              stream: bloc.balanceSchemeStream,
              builder: (context, snapshot) {
                return SizedBox(
                  child: snapshot.hasData
                      ? BalanceSchemeInfo(
                          balanceScheme: snapshot.data,
                        )
                      : const SizedBox(
                          child: Text(
                            Names.noData,
                            textAlign: TextAlign.center,
                          ),
                          width: 300,
                        ),
                );
              }),
        ],
      ),
    );
  }
}
