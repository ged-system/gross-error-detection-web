import 'dart:math';

import 'package:flutter/material.dart';
import 'package:graphview/GraphView.dart';
import 'package:provider/provider.dart';

import 'package:ged/features/error_reduction/presentation/error_reduction_bloc.dart';

import 'package:ged/features/error_reduction/data/data.dart';

class GraphClusterViewPage extends StatefulWidget {
  const GraphClusterViewPage({Key? key}) : super(key: key);

  @override
  State<GraphClusterViewPage> createState() => _GraphClusterViewPageState();
}

class _GraphClusterViewPageState extends State<GraphClusterViewPage> {
  ErrorReductionBloc get bloc => Provider.of<ErrorReductionBloc>(context, listen: false);

  var color = Colors.redAccent;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BalanceScheme>(
        stream: bloc.balanceSchemeStream,
        builder: (context, balanceSchemeSnapshot) {
          final balanceScheme = balanceSchemeSnapshot.data;

          final flows = balanceScheme?.flows.toList();
          if (flows == null) {
            return Container();
          }
          
          final uNodes = <String>{};
          for (var entity in flows) {
            final sourceId = entity.sourceId;
            final destinationId = entity.destinationId;
            
            if (sourceId != null) {
              uNodes.add(sourceId);
            }
            
            if (destinationId != null) {
              uNodes.add(destinationId);
            }
            
          }

          // Создать стэк с ОК узлами
          // и когда буду добавлять потоки убирать узел из стэка
          final nodes = uNodes.map((e) => _getNode(e)).toList();
          
          nodes.addAll(flows.where((entity) => entity.sourceId == null || entity.destinationId == null).map((e) => _getNode('ОК')));
          return Container();
        });

    // final Graph graph = Graph();
    //
    // final a = Node(Tooltip(
    //   message: 'один',
    //   child: Container(
    //       decoration: BoxDecoration(
    //         color: color,
    //         border: Border.all(), // Set border width
    //         borderRadius: const BorderRadius.all(Radius.circular(45.0)),
    //       ),
    //       width: 50,
    //       height: 50,
    //       child: const Center(child: Text('один'))),
    // ));
    // final b = Node(const Text('два'));
    // final c = Node(const Text('три'));
    // final zero1 = Node(const Text('ОК'));
    // final zero2 = Node(const Text('ОК'));
    // final zero3 = Node(const Text('ОК'));
    // final zero4 = Node(const Text('ОК'));
    // final zero5 = Node(const Text('ОК'));
    //
    // graph.addEdge(zero1, a);
    // graph.addEdge(a, zero2);
    // graph.addEdge(a, b, paint: Paint()..color = Colors.red);
    // graph.addEdge(a, b);
    // graph.addEdge(b, zero3);
    // graph.addEdge(b, c);
    // graph.addEdge(c, zero4);
    // graph.addEdge(c, zero5);
    //
    // return Column(
    //   children: [
    //     Expanded(
    //       child: InteractiveViewer(
    //           constrained: false,
    //           boundaryMargin: const EdgeInsets.all(8),
    //           minScale: 0.001,
    //           maxScale: 100,
    //           child: GraphView(
    //               graph: graph,
    //               algorithm: SugiyamaAlgorithm(builder),
    //               paint: Paint()
    //                 ..color = Colors.green
    //                 ..strokeWidth = 1
    //                 ..style = PaintingStyle.fill,
    //               builder: (Node node) {
    //                 // I can decide what widget should be shown here based on the id
    //                 var a = node.key!.value as int?;
    //                 if (a == 2) {
    //                   return rectangWidget(a);
    //                 }
    //                 return rectangWidget(a);
    //               })),
    //     ),
    //   ],
    // );
  }

  int n = 8;
  Random r = Random();

  Widget rectangWidget(int? i) {
    return Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          boxShadow: const [
            BoxShadow(color: Colors.blue, spreadRadius: 1),
          ],
        ),
        child: Text('Node $i'));
  }

  Node _getNode(String name) {
    return Node(Tooltip(
      message: name,
      child: Container(
          decoration: BoxDecoration(
            color: color,
            border: Border.all(), // Set border width
            borderRadius: const BorderRadius.all(Radius.circular(45.0)),
          ),
          width: 50,
          height: 50,
          child: const Center(child: Text('один'))),
    ));
  }

  late SugiyamaConfiguration builder = SugiyamaConfiguration();

  @override
  void initState() {
    builder
      ..nodeSeparation = (30)
      ..levelSeparation = (50)
      ..orientation = SugiyamaConfiguration.ORIENTATION_LEFT_RIGHT;
  }
}
