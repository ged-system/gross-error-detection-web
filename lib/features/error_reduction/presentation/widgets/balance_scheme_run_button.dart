import 'package:flutter/material.dart';

class BalanceSchemeRunButton extends StatelessWidget {
  final String label;
  final VoidCallback? onPressed;

  const BalanceSchemeRunButton({
    Key? key,
    required this.onPressed,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(primary: Colors.indigoAccent);

    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: ElevatedButton(
          child: Text(label),
          style: style,
          onPressed: onPressed,
        ),
      ),
    );
  }
}
