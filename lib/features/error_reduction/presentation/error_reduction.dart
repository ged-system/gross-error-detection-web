import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/presentation/presentation.dart';
import 'package:provider/provider.dart';

class ErrorReduction extends StatelessWidget {
  const ErrorReduction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (BuildContext ctx) => ErrorReductionBloc()..init(),
      dispose: (BuildContext ctx, ErrorReductionBloc bloc) => bloc.dispose(),
      child: const ErrorReductionView(),
    );
  }
}
