import 'package:flutter/material.dart';
import 'package:ged/features/error_reduction/presentation/presentation.dart';

class ErrorReductionView extends StatelessWidget {
  const ErrorReductionView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Gross Error Detection Service'),
        ),
        body: Container(
          alignment: Alignment.topCenter,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: const [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: BalanceSchemeArea(),
                ),
                // SizedBox(width: 300, height: 300, child: GraphClusterViewPage()),
                InteractiveArea(),
                ResultedArea(),
              ],
            ),
          ),
        ));
  }
}
