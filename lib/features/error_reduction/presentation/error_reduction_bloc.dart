import 'dart:async';
import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:file_picker/file_picker.dart';
import 'package:ged/core/library/utils/data_stack.dart';
import 'package:http/http.dart';

import 'package:ged/features/error_reduction/error_reduction.dart';

import 'package:ged/core/core.dart';

class ErrorReductionBloc implements BaseBloc {
  @override
  void init() {
    _dataStack = DataStack();
    _balanceSchemeController = StreamController<BalanceScheme>.broadcast();
    _gedInteractiveController = StreamController<GedResponseDto>.broadcast();
    _selectedFlowController = StreamController<GlrSampleFlowDto>.broadcast();
    _requestStatusController = StreamController<RequestStatusEnum>.broadcast();
    _reduceErrorsController = StreamController<GedReductionResponseDto>.broadcast();
  }

  @override
  void dispose() {
    _balanceSchemeController.close();
    _gedInteractiveController.close();
    _selectedFlowController.close();
    _requestStatusController.close();
    _reduceErrorsController.close();
  }

  bool hideInteractiveArea = true;
  bool hideResultedArea = true;

  /// Контроллер объекта балансовой схемы
  late StreamController<BalanceScheme> _balanceSchemeController;

  /// Поток объекта балансовой схемы
  Stream<BalanceScheme> get balanceSchemeStream => _balanceSchemeController.stream;

  /// Контроллер интерактивного поиска грубых ошибок
  late StreamController<GedResponseDto> _gedInteractiveController;

  /// Поток контроллера интерактивного поиска грубых ошибок
  Stream<GedResponseDto> get gedInteractiveStream => _gedInteractiveController.stream;

  /// Контроллер выбранного потока
  late StreamController<GlrSampleFlowDto> _selectedFlowController;

  /// Поток контроллера выбранного потоке
  Stream<GlrSampleFlowDto> get selectedFlowStream => _selectedFlowController.stream;

  /// Контроллер статуса запроса
  late StreamController<RequestStatusEnum> _requestStatusController;

  /// Поток статуса запроса
  Stream<RequestStatusEnum> get requestStatusStream => _requestStatusController.stream;

  /// Контроллер устранения грубых ошибок
  late StreamController<GedReductionResponseDto> _reduceErrorsController;

  /// Поток контроллера устранения грубых ошибок
  Stream<GedReductionResponseDto> get reduceErrorsStream => _reduceErrorsController.stream;

  late DataStack<GedResponseDto> _dataStack;

  /// Выбор файла балансовой схемы
  void pickFiles() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['json'],
    );

    if (result == null) {
      return;
    }

    // Получаем данные из файлы в виде байтов
    final fileBytes = result.files.first.bytes;
    final fileName = result.files.first.name;

    if (fileBytes == null) {
      throw Exception(Exceptions.fileDataException);
    }

    // Конвертируем байты в строку формата JSON
    final data = String.fromCharCodes(fileBytes.toList());

    // Декодируем данные
    final jsonDecoded = jsonDecode(data);

    final balanceScheme = serializers.deserializeWith(BalanceSchemeDto.serializer, jsonDecoded);

    if (balanceScheme == null) {
      throw Exception(Exceptions.jsonException);
    }

    final flows = balanceScheme.flows?.map((entity) => Flow((builder) => builder
      ..id = entity.id
      ..name = entity.name
      ..destinationId = entity.destinationId
      ..sourceId = entity.sourceId
      ..measured = entity.measured
      ..tolerance = entity.tolerance
      ..isMeasured = entity.isMeasured
      ..isExcluded = entity.isExcluded
      ..metrologicRange = FlowConstraints((b) => b
        ..upperBound = entity.metrologicRange?.upperBound
        ..lowerBound = entity.metrologicRange?.lowerBound).toBuilder()
      ..technologicRange = FlowConstraints((b) => b
        ..upperBound = entity.technologicRange?.upperBound
        ..lowerBound = entity.technologicRange?.lowerBound).toBuilder()
      ..isArtificial = entity.isArtificial));

    if (flows == null) {
      throw Exception(Exceptions.jsonException);
    }

    final obj = BalanceScheme((builder) => builder
      ..name = fileName
      ..flows = ListBuilder(flows));

    hideResultedArea = true;
    hideInteractiveArea = true;

    _balanceSchemeController.sink.add(obj);
    _gedInteractiveController.sink.add(GedResponseDto());
    _reduceErrorsController.sink.add(GedReductionResponseDto());
  }

  /// Начать поиск грубых оишбок
  Future<void> startGed(BalanceScheme balanceScheme) async {
    final url = Uri.parse('${Urls.serverUrl}/${Urls.grossErrorDetectionInteractive}');
    final headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };

    final flows = balanceScheme.flows
        .map((flow) => FlowDto((builder) => builder
          ..id = flow.id
          ..name = flow.name
          ..destinationId = flow.destinationId
          ..sourceId = flow.sourceId
          ..measured = flow.measured
          ..tolerance = flow.tolerance
          ..isMeasured = flow.isMeasured
          ..isExcluded = flow.isExcluded
          ..metrologicRange = ConstraintsDto((b) => b
            ..upperBound = flow.metrologicRange?.upperBound
            ..lowerBound = flow.metrologicRange?.lowerBound).toBuilder()
          ..technologicRange = ConstraintsDto((b) => b
            ..upperBound = flow.technologicRange?.upperBound
            ..lowerBound = flow.technologicRange?.lowerBound).toBuilder()
          ..isArtificial = flow.isArtificial))
        .toBuiltList();

    final body = jsonEncode(GedRequestDto((builder) => builder..flows = flows.toBuilder()).toJson());

    _requestStatusController.sink.add(RequestStatusEnum.load);

    final response = await post(
      url,
      headers: headers,
      body: body,
    );

    if (response.statusCode != 200) {
      _requestStatusController.sink.add(RequestStatusEnum.error);
      return;
    }

    // Конвертируем байты в строку формата JSON
    final data = String.fromCharCodes(response.bodyBytes.toList());

    // Декодируем данные
    final jsonDecoded = jsonDecode(data);

    final result = serializers.deserializeWith(GedResponseDto.serializer, jsonDecoded);

    if (result == null) {
      return;
    }
    _requestStatusController.sink.add(RequestStatusEnum.work);

    hideResultedArea = true;
    hideInteractiveArea = false;

    _dataStack.clear();
    _dataStack.add(result);

    _gedInteractiveController.sink.add(result);
    _reduceErrorsController.sink.add(GedReductionResponseDto());
  }

  /// Продолжить поиск грубых ошибок
  Future<void> continueGed(
      List<FlowDto> balanceSchemeFlows, List<FlowDto> errorFlows, GlrSampleFlowDto selectedFlow) async {
    final url = Uri.parse('${Urls.serverUrl}/${Urls.grossErrorDetectionInteractive}');
    final headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };

    final flows = balanceSchemeFlows
        .map((flow) => FlowDto((builder) => builder
          ..id = flow.id
          ..name = flow.name
          ..destinationId = flow.destinationId
          ..sourceId = flow.sourceId
          ..measured = flow.measured
          ..tolerance = flow.tolerance
          ..isMeasured = flow.isMeasured
          ..isExcluded = flow.isExcluded
          ..metrologicRange = ConstraintsDto((b) => b
            ..upperBound = flow.metrologicRange?.upperBound
            ..lowerBound = flow.metrologicRange?.lowerBound).toBuilder()
          ..technologicRange = ConstraintsDto((b) => b
            ..upperBound = flow.technologicRange?.upperBound
            ..lowerBound = flow.technologicRange?.lowerBound).toBuilder()
          ..isArtificial = flow.isArtificial))
        .toBuiltList();

    final body = jsonEncode(GedRequestDto((builder) => builder
      ..flows.replace(flows)
      ..errorFlows.replace(errorFlows)
      ..sampleFlow.replace(selectedFlow)).toJson());

    _requestStatusController.sink.add(RequestStatusEnum.load);

    final response = await post(
      url,
      headers: headers,
      body: body,
    );

    if (response.statusCode != 200) {
      return;
    }

    // Конвертируем байты в строку формата JSON
    final data = String.fromCharCodes(response.bodyBytes.toList());

    // Декодируем данные
    final jsonDecoded = jsonDecode(data);

    final result = serializers.deserializeWith(GedResponseDto.serializer, jsonDecoded);

    if (result == null) {
      return;
    }

    selectFlow(GlrSampleFlowDto());

    _requestStatusController.sink.add(RequestStatusEnum.work);

    hideResultedArea = true;
    hideInteractiveArea = false;

    _dataStack.add(result);

    _gedInteractiveController.sink.add(result);
  }

  /// Выполнить устранение грубых ошибок
  Future<void> reduceErrors(List<FlowDto> balanceSchemeFlows, List<FlowDto> errorFlows) async {
    final url = Uri.parse('${Urls.serverUrl}/${Urls.grossErrorReductionInteractive}');
    final headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };

    final flows = balanceSchemeFlows
        .map((flow) => FlowDto((builder) => builder
          ..id = flow.id
          ..name = flow.name
          ..destinationId = flow.destinationId
          ..sourceId = flow.sourceId
          ..measured = flow.measured
          ..tolerance = flow.tolerance
          ..isMeasured = flow.isMeasured
          ..isExcluded = flow.isExcluded
          ..metrologicRange = ConstraintsDto((b) => b
            ..upperBound = flow.metrologicRange?.upperBound
            ..lowerBound = flow.metrologicRange?.lowerBound).toBuilder()
          ..technologicRange = ConstraintsDto((b) => b
            ..upperBound = flow.technologicRange?.upperBound
            ..lowerBound = flow.technologicRange?.lowerBound).toBuilder()
          ..isArtificial = flow.isArtificial))
        .toBuiltList();

    final body = jsonEncode(GedRequestDto((builder) => builder
      ..flows.replace(flows)
      ..errorFlows.replace(errorFlows)).toJson());

    final response = await post(
      url,
      headers: headers,
      body: body,
    );

    if (response.statusCode != 200) {
      return;
    }

    // Конвертируем байты в строку формата JSON
    final data = String.fromCharCodes(response.bodyBytes.toList());

    // Декодируем данные
    final jsonDecoded = jsonDecode(data);

    final result = serializers.deserializeWith(GedReductionResponseDto.serializer, jsonDecoded);

    if (result == null) {
      return;
    }
    hideResultedArea = false;
    hideInteractiveArea = false;

    _dataStack.clear();
    _reduceErrorsController.sink.add(result);
  }

  void selectFlow(GlrSampleFlowDto flow) {
    _selectedFlowController.sink.add(flow);
  }

  void stepBack() {
    var value = _dataStack.receive();
    if (value == null) {
      return;
    }

    if (_dataStack.length() == 0) {
      _dataStack.add(value);
      _gedInteractiveController.sink.add(value);
      return;
    }

    value = _dataStack.receive();
    if (value == null) {
      return;
    }

    if (_dataStack.length() == 0) {
      _dataStack.add(value);
    }
    _gedInteractiveController.sink.add(value);
  }
}
